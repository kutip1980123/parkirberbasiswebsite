<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="Hanif" content="width=device-width, initial-scale=1.0" />
  <title>Panduan</title>
  <link rel="stylesheet" href="style2.css" />
  <script src="https://kit.fontawesome.com/a80154db7c.js" crossorigin="anonymous"></script>
</head>

<body>
  <?php include "components/navbar.php" ?>

  <section id="PANDUAN">
    <div class="logo">
      <p></p>
    </div>
    <div class="kolom">
      <p class="Deskripsi">PENJELASAN TARIF IMPOR</p>
      <h3>Tarif Normal Pajak Impor/Bea Masuk</h3>
      <p>
        Pemerintah telah menetapkan tarif Bea Masuk normal untuk komoditi tas,
        sepatu, dan garmen sebesar:
      </p>
      <ol>
        <li>Tas khusus 15%-20%</li>
        <li>Sepatu khusus 15%-25%</li>
        <li>Produk tekstil dengan PPN 11%</li>
        <li>Serta PPh Pasal 22 impor sebesar 7,5% hingga 10%</li>
      </ol>
    </div>
  </section>

  <?php include "components/footer.php" ?>
  <script src="./script.js"></script>
</body>

</html>