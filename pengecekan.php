<?php
require './pengecekanbea.php';
require_once './config/connection.php';

if (isset($_GET['submit'])) {
	$bea = beaCukai();
	$pajak_lainnya = pajakImport();

	$total = totalPajak($bea, $pajak_lainnya);

	$sql = "SELECT SUM(etika, membaca, menulis, kreatif) FROM ";
	$sql = "INSERT INTO rincian_pembayaran VALUES ('', '$bea', '$pajak_lainnya[0]', '$pajak_lainnya[1]', '$pajak_lainnya[2]', '$total')";
	$result = $conn->query($sql);
	if ($result) {
		echo "<script>alert('data berhasil dimasukkan')</script>";
	} else {
		echo "<script>alert('data gagal di input')</script>";
	}
}
?>


<!-- @format -->

<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="Hanif" content="width=device-width, initial-scale=1.0" />
	<title>Login</title>
	<link rel="stylesheet" href="style3.css" />
	<script src="https://kit.fontawesome.com/a80154db7c.js" crossorigin="anonymous"></script>
	<link href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" rel="stylesheet" />
</head>

<body>
	<nav>
		<div class="wrapper">
			<div class="logo"><a href="">Pengecekan Tarif Bea Cukai</a></div>
			<div class="menu">
				<ul>
					<?php if ($_SERVER['PHP_SELF'] === "index.php") { ?>
						<li><a href="#">HOME</a></li>
					<?php } else { ?>
						<li><a href="index.php">HOME</a></li>
					<?php } ?>
					<li><a href="panduan.php">PANDUAN</a></li>
					<li><a href="about.php">ABOUT</a></li>
					<li>
						<a href="pengecekan.php" class="tbl-merah">PENGECEKAN TARIF</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>


	<div class="bea_cukai">
		<form id="form" method="GET">
			<h1>Cek Harga Tarif Impor Bea Cukai</h1>
			<div class="form-control">
				<label for="Harga Barang" id="label-name">Masukan Harga Barang(Sesuai Mata uang dari negara
					tersebut)</label>
				<input type="number" name="harga_barang" id="harga_barang" />
			</div>

			<div class="form-control">
				<label for="Ongkos Kirim" id="label-name">Masukan Ongkos kirim(sesuai mata uang dari negara
					tersebut)</label>
				<input type="number" name="ongkos_kirim" id="ongkos_kirim" />
			</div>

			<div class="form-control">
				<label for="Asuransi" id="label-name">Masukan Ansuransi(Jika tidak ada masukan angka 0)</label>
				<input type="number" name="asuransi" id="asuransi" />
			</div>

			<div class="form-control">
				<label for="Kurs Mata Uang" id="label-name">Masukan Kurs Mata Uang (Sesuai dengan asal negara barang
					tersebut)</label>
				<input type="number" name="kurs" id="kurs" />
			</div>

			<div class="form-control">
				<label for="Tarif bea" id="label-name">Tarif bea masuk(dapat dilihat pada halaman tarif Bea
					cukai)</label>
				<input type="number" name="bea" id="bea" />
			</div>
			<!-- </form> -->
			<!-- <form id="form" method="GET"> -->
			<h1>Perhitungan Pajak dari Import</h1>
			<div class="form-control">
				<label for="Tarif PPN" id="label-name">Tarif PPN</label>
				<input type="number" name="tarif_ppn" id="tarif_ppn" />
			</div>
			<div class="form-control">
				<div class="">
					<input type="radio" id="Non_NPWP_15%" name="tarif-import" value="15%" />
					<label for="Non">Non NPWP 15%</label><br />
				</div>
				<div class="">
					<input type="radio" id="NPWP_non_API_7,5%" name="tarif-import" value="7,5%" />
					<label for="npwp">NPWP/non API 7,5%</label><br />
				</div>
				<div class="">
					<input type="radio" id="API_2,5%" name="tarif-import" value="2,5%" />
					<label for="api">API 2,5%</label><br />
				</div>
			</div>
			<div class="form-control">
				<label for="Tarif PPnBM" id="label-name">Tarif PPnBM</label>
				<input type="number" name="tarif-ppnbm" min="1" max="100" />
			</div>
			<button type="submit" name="submit">Submit</button>
		</form>

		<form id="form" method="POST">
			<h1>Rincian Pembayaran</h1>
			<?php if (isset($bea) && isset($pajak_lainnya)) { ?>
				<div class="form-control">
					<label for="Total Bea Masuk" id="label name">Total Bea Masuk</label>
					<input type="number" name="Total Bea Masuk" id="label" value="<?php echo $bea ?>">
				</div>

				<div class="form-control">
					<label for="Total PPN" id="label name">Total PPN</label>
					<input type="number" name="Total PPN" id="label" value="<?php echo $pajak_lainnya[0] ?>">
				</div>

				<div class="form-control">
					<label for="Total PPh" id="label name">Total PPh</label>
					<input type="text" name="Total PPh" id="label" value="<?php echo $pajak_lainnya[1] ?>">
				</div>

				<div class="form-control">
					<label for="Total  PPnBM" id="label name">Total PPnBM</label>
					<input type="number" name="Total  PPnBM" id="label" value="<?php echo $pajak_lainnya[2] ?>">
				</div>
			<?php } else { ?>
				<div class="form-control">
					<label for="Total Bea Masuk" id="label name">Total Bea Masuk</label>
					<input type="number" name="Total Bea Masuk" id="label" value="<?php echo $bea ?>">
				</div>

				<div class="form-control">
					<label for="Total PPN" id="label name">Total PPN</label>
					<input type="number" name="Total PPN" id="label">
				</div>

				<div class="form-control">
					<label for="Total PPh" id="label name">Total PPh</label>
					<input type="text" name="Total PPh" id="label">
				</div>

				<div class="form-control">
					<label for="Total  PPnBM" id="label name">Total PPnBM</label>
					<input type="number" name="Total  PPnBM" id="label">
				</div>
			<?php } ?>
		</form>

		<form action="#" id="form">
			<h1>Total Pembayaran</h1>
			<div class="form-control">
				<label for="Total Pembayaran" id="label">Tota Bayar</label>
				<input type="number" name="Total Bayar" id="label" value="<?php echo $total ?>">
			</div>
		</form>
	</div>

	<!-- <div class="bea_cukai">
		<h1>Histori</h1>
		<table>
			<thead>
				<tr>
					<th>Total Bea Masuk</th>
					<th>Total PPN</th>
					<th>Total PPh</th>
					<th>Total PPnBM</th>
					<th>Total Pembayaran</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$sql_query = "SELECT * FROM rincian_pembayaran";
				$result = $conn->query($sql_query);

				if ($result->num_rows > 0) {
					while ($data = $result->fetch_assoc()) {
				?>
						<td><?php echo $data['total_bea_masuk'] ?></td>
						<td><?php echo $data['total_PPN'] ?></td>
						<td><?php echo $data['total_PPh'] ?></td>
						<td><?php echo $data['total_PPnBM'] ?></td>
						<td><?php echo $data['pembayaran'] ?></td>
					<?php
					}
				} else {
					?>
					<p>Data Tidak Ditemukan</p>
				<?php } ?>
			</tbody>
		</table> -->
	</div>

	<?php include "components/footer.php" ?>

	<script src="./pengecekan.js"></script>
</body>

</html>