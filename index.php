<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="Hanif" content="width=device-width, initial-scale=1.0" />
  <title>Parkir</title>
  <link rel="stylesheet" href="style.css" />
</head>
<script src="https://kit.fontawesome.com/a80154db7c.js" crossorigin="anonymous"></script>
<link href="https://unpkg.com/boxicons@2.1.4/css/boxicons.min.css" rel="stylesheet" />

<body>
  <?php include "components/navbar.php" ?>

  <div class="header" id="HOME">
    <!--Untuk Home-->
    <div class="header-img">
      <img src="img1.jpg" />
    </div>
    <div class="kolom">
      <h1 class="deskripsi">SELAMAT DATANG DI</h1>
      <h2>Pengecekan BeaCukai</h2>
      <p>Website ini anda dapat melakukan pengecekan total bea cukai</p>
    </div>
  </div>

  <?php include "components/footer.php" ?>
  <script src="script.js"></script>
</body>

</html>