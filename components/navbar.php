<nav>
    <div class="wrapper">
        <div class="logo"><a href="">Pengecekan Tarif Bea Cukai</a></div>
        <div class="menu">
            <ul>
                <?php if ($_SERVER['PHP_SELF'] === "index.php") { ?>
                    <li><a href="#">HOME</a></li>
                <?php } else { ?>
                    <li><a href="index.php">HOME</a></li>
                <?php } ?>
                <li><a href="panduan.php">PANDUAN</a></li>
                <li><a href="about.php">ABOUT</a></li>
                <li>
                    <a href="pengecekan.php" class="tbl-merah">PENGECEKAN TARIF</a>
                </li>
            </ul>
            <div class="menu-bar">
                <button type="submit" class="button">
                    <i class="fas fa-solid fa-bars"></i>
                </button>
            </div>
        </div>
    </div>
</nav>