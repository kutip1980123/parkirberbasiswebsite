-- --------------------------------------------------------
-- Host:                         localhost
-- Server version:               5.1.72-community - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for bea_cukai
CREATE DATABASE IF NOT EXISTS `bea_cukai` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bea_cukai`;

-- Dumping structure for table bea_cukai.rincian_pembayaran
CREATE TABLE IF NOT EXISTS `rincian_pembayaran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `total_bea_masuk` bigint(20) NOT NULL DEFAULT '0',
  `total_PPN` int(11) NOT NULL DEFAULT '0',
  `total_PPh` int(11) NOT NULL DEFAULT '0',
  `total_PPnBM` int(11) NOT NULL DEFAULT '0',
  `pembayaran` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table bea_cukai.rincian_pembayaran: 1 rows
/*!40000 ALTER TABLE `rincian_pembayaran` DISABLE KEYS */;
INSERT INTO `rincian_pembayaran` (`id`, `total_bea_masuk`, `total_PPN`, `total_PPh`, `total_PPnBM`, `pembayaran`) VALUES
	(1, 24000000000, 2000, 15, 20, 24000002020);
/*!40000 ALTER TABLE `rincian_pembayaran` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
