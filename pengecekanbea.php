<?php

function beaCukai()
{
    $hargaBarang = $_GET['harga_barang'];
    $ongkosKirim = $_GET['ongkos_kirim'];
    $asuransi = $_GET['asuransi'];
    $kurs = $_GET['kurs'];
    $bea = $_GET['bea'];

    $totalBEA = (($hargaBarang + $ongkosKirim + $asuransi) * $kurs * $bea);

    return $totalBEA;
}

function pajakImport()
{
    $tarifPPN = $_GET['tarif_ppn'];
    $tarifImport = $_GET['tarif-import'];
    $tarifPPnBM = $_GET['tarif-ppnbm'];

    return [$tarifPPN, $tarifImport, $tarifPPnBM];
}

function totalPajak($a, $b)
{

    if ($b[1] == '15%') {
        $persen = 0.15;
        $total_pembayaran = $a + $b[0] + $persen + $b[2];

        return $total_pembayaran;
    } else if ($b[1] == '7,5%') {
        $persen = 0.075;
        $total_pembayaran = $a + $b[0] + $persen + $b[0];

        return $total_pembayaran;
    } else if ($b[1] == '2,5%') {
        $persen = 0.025;
        $total_pembayaran = $a + $b[0] + $persen + $b[0];

        return $total_pembayaran;
    }

    return;
}
