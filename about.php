<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <title>about</title>
  <link rel="stylesheet" href="style4.css" />
  <script src="https://kit.fontawesome.com/a80154db7c.js" crossorigin="anonymous"></script>
</head>

<body>
  <?php include "components/navbar.php" ?>

  <div class="wrapper">
    <section id="about.html">
      <div class="kolom">
        <h1 class="Deskripsi">Deskripsi</h1>
        <h3>Bea Cukai Indonesia</h3>
        <p class="pengertian">
          Cukai adalah pungutan negara yang dikenakan terhadap barang-barang
          tertentu yang mempunyai sifat atau karakteristik yang ditetapkan
          dalam Undang-undang Cukai.
        </p>
        <p class="pasal">
          Pasal 13 Ayat (1) Ayat ini memberikan kewenangan kepada Menteri
          untuk menetapkan tarif Bea Masuk yang besarnya berbeda dengan tarif
          yang dimaksud dalam Pasal 12 ayat (1).
        </p>
        <ul>
          <li>
            <p>
              <b> Huruf a</b>. Tarif Bea Masuk dikenakan berdasarkan
              perjanjian atau kesepakatan yang dilakukan Pemerintah Republik
              Indonesia dengan pemerintah negara lain atau beberapa negara
              lain, misalnya Bea Masuk berdasarkan Common Effective
              Preferential Tariff untuk Asean Free Trade Area (CEPT for AFTA).
            </p>
          </li>
          <li>
            <p>
              <b> Huruf b</b>. Dalam rangka mempermudah dan mempercepat
              penyelesaian impor barang bawaan penumpang, awak sarana
              pengangkut, pelintas batas, dan barang kiriman melalui pos atau
              jasa titipan, dapat dikenakan Bea Masuk berdasarkan tarif yang
              berbeda dengan tarif sebagaimana dimaksud dalam Pasal 12 ayat
              (1), misalnya dengan pengenaan tarif rata-rata. Ketentuan ini
              perlu, mengingat barang-barang yang dibawa oleh para penumpang,
              awak sarana pengangkut, dan pelintas batas pada umumnya terdiri
              dari beberapa jenis.
            </p>
          </li>
          <li>
            <p>
              <b> Huruf c</b>. Dalam hal barang ekspor Indonesia diperlakukan
              secara tidak wajar oleh suatu negara misalnya dengan pembatasan,
              larangan, atau pengenaan tambahan Bea Masuk, barang-barang dari
              negara yang bersangkutan dapat dikenakan tarif yang besarnya
              berbeda dengan tarif yang dimaksud dalam Pasal 12 ayat (1).
            </p>
          </li>
        </ul>
      </div>
    </section>
  </div>

  <?php include "components/footer.php" ?>
  <script src="./script.js"></script>
</body>

</html>